package gksh.smev.gateway.client;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.shuffle.smev.gateway.client.SmevGatewayClient;
import ru.shuffle.smev.gateway.client.dto.AttachDto;
import ru.shuffle.smev.gateway.client.dto.AttachSpecificationDto;
import ru.shuffle.smev.gateway.client.dto.NotificationDto;
import ru.shuffle.smev.gateway.client.dto.ResponseDto;

public class SmevGatewayClientTest {

    private static final String AUTH_TOKEN = "qASDwel231ASDF";

    private static final String MNEMONICS = "lipetsk-test";

    private static final String EP_RUN_EGRUL = "urn://x-artefacts-fns-vipul-tosmv-ru/311-14/4.0.6";

    private final SmevGatewayClient client = new SmevGatewayClient("http://82.146.46.225:8081");

    @Test(enabled = false)
    public void testCreateAttach() {
        String state = "state";
        AttachSpecificationDto spec = new AttachSpecificationDto();
        spec.contentType = "application/xml";
        spec.exchangeProtocolURN = EP_RUN_EGRUL;
        spec.fileName = "egrul-request.xml";
        ResponseDto<AttachDto> response = client.createAttach(AUTH_TOKEN, MNEMONICS, state, spec);
        SmevGatewayClient.dumpJson(response);
        Assert.assertTrue(response.success);
        Assert.assertEquals(response.state, state);
        Assert.assertEquals(response.mnemonics, MNEMONICS);
    }

    @Test(enabled = false)
    void testFindAttachById() {
        ResponseDto<AttachDto> response = client.findAttachById(AUTH_TOKEN, MNEMONICS, null, 1);
        SmevGatewayClient.dumpJson(response);
    }

    @Test(enabled = false)
    void testListNotifications() {
        ResponseDto<NotificationDto[]> response = client.listNotifications(AUTH_TOKEN, MNEMONICS, null, 10, 0);
        SmevGatewayClient.dumpJson(response);
    }

    @Test(enabled = false)
    void testUploadAttachContent() throws Exception {
        final AttachDto attach;
        {//create attach
            AttachSpecificationDto spec = new AttachSpecificationDto();
            spec.contentType = "application/xml";
            spec.exchangeProtocolURN = EP_RUN_EGRUL;
            spec.fileName = "egrul-request.xml";
            ResponseDto<AttachDto> response = client.createAttach(AUTH_TOKEN, MNEMONICS, null, spec);
            SmevGatewayClient.dumpJson(response);
            Assert.assertTrue(response.success);
            attach = response.result;
        }
        File attachFile = new File("src/test/resources/egrul-request.xml");
        {//upload attach content
            ResponseDto<AttachDto> response = client.uploadAttachContent(AUTH_TOKEN, MNEMONICS, null, attach.id, new FileInputStream(attachFile));
            SmevGatewayClient.dumpJson(response);
            Assert.assertTrue(response.success);
            Assert.assertEquals(response.result.id, attach.id);
            Assert.assertEquals((long) response.result.contentLength, attachFile.length());
        }
        //download attach content
        {
            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                ResponseDto<Void> response = client.downloadAttachContent(AUTH_TOKEN, MNEMONICS, null, attach.id, outputStream);
                Assert.assertEquals(outputStream.toByteArray().length, attachFile.length());
                Assert.assertTrue(response.success);
            }
        }
    }

}
