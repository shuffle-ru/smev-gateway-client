package ru.shuffle.smev.gateway.client.dto;

public enum AttachTypeDto {
        //@Schema(description = "attach клиента")
        CLIENT,
        //@Schema(description = "attach из СМЭВ")
        SMEV;
}
