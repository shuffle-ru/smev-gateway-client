package ru.shuffle.smev.gateway.client.dto;

import java.util.UUID;
import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class MessageAsyncProcessingStatusDto {

    //@Schema(description = "Идентификатор сообщения (//OriginalMessageId) сформированный отправителем сообщения", required = true)
    @NotNull
    public UUID originalMessageId;

    //@Schema(description = "Уведомление об описании статуса сообщения, содержащий описание статуса сообщения")
    @Nullable
    public String statusDetails;

    //@Schema(description = "Категория статуса")
    @Nullable
    public MessageStatusCategoryDto statusCategory;

    @Valid
    @Nullable
    public MessageFaultDto smevFault;

}