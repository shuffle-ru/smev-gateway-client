package ru.shuffle.smev.gateway.client.dto;

public enum NotificationTypeDto {
        //        @Schema(description = "СМЭВ  принял запрос")
        SMEV_ACCEPT,
        //      @Schema(description = "СМЭВ не принял запрос")
        SMEV_ERROR,
        //    @Schema(description = "Ответ от СМЭВ")
        SMEV_RESPONSE,
        //  @Schema(description = "Запрос от СМЭВ")
        SMEV_REQUEST,
        //@Schema(description = "Пришло сообщение в очередь статусов СМЭВ")
        SMEV_NOTIFICATION,
        //@Schema(description = "Ошибка при отправке в СМЭВ")
        SMEV_SEND_ERROR;
}