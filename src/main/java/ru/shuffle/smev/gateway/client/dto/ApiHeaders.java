package ru.shuffle.smev.gateway.client.dto;

public class ApiHeaders {

    public static final String AUTH = "x-sgw-auth";

    public static final String MNEMONICS = "x-sgw-mnemonics";

    public static final String STATE = "x-sgw-state";

    public static final String IS_TEST_LOOPBACK = "x-sgw-test-loopback";

}
