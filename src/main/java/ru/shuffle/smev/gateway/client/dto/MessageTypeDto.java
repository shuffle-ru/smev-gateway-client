package ru.shuffle.smev.gateway.client.dto;

public enum MessageTypeDto {
        //        @Schema(description = "Запрос клиента")
        CLIENT_REQUEST,
        //      @Schema(description = "Ответ от СМЭВ")
        SMEV_RESPONSE,
        //    @Schema(description = "Запрос от СМЭВ")
        SMEV_REQUEST,
        //  @Schema(description = "Ответ от клиента, на запрос СМЭВ")
        CLIENT_RESPONSE,
        //@Schema(description = "Cообщение из очереди статусов СМЭВ")
        SMEV_NOTIFICATION;
}