package ru.shuffle.smev.gateway.client.dto;

import java.util.Map;
import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class MessageRequestStatusDto {

    //    @Schema(description = "Код бизнес-статуса запроса", required = true)
    public int statusCode;

    @NotBlank
    //  @Schema(description = "Описание бизнес-статуса запроса, в человекочитаемом виде", required = true)
    public String statusDescription;

    @Nullable
    //    @Schema(description = "Статус может сопровождаться неограниченным количеством параметров, которые описываются парами «ключ-значение»")
    @JsonDeserialize(keyAs = String.class, contentAs = String.class)
    @JsonSerialize(keyAs = String.class, contentAs = String.class)
    public Map<String, String> statusParameters;

}