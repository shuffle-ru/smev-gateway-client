package ru.shuffle.smev.gateway.client.dto;

public enum AttachStatusDto {
        //@Schema(description = "Клиент создал attach")
        CLIENT_NEW,
        //@Schema(description = "Клиент загрузил контент attach-а")
        CLIENT_UPLOAD_SUCCESS,
        //@Schema(description = "Attach загружен на СМЭВ FTP")
        SMEV_UPLOAD_SUCCESS,
        //@Schema(description = "Ошибка при загрузке attach-а в СМЭВ FTP")
        SMEV_UPLOAD_FAIL,
        //@Schema(description = "СМЭВ создал attach")
        SMEV_NEW,
        //@Schema(description = "Attach скачан с СМЭВ FTP - или из XML")
        SMEV_DOWNLOAD_SUCCESS,
        //@Schema(description = "Ошибка при скачивании attach-а из СМЭВ FTP или из XML")
        SMEV_DOWNLOAD_FAIL;
}
