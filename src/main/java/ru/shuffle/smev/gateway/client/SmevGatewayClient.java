package ru.shuffle.smev.gateway.client;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import javax.annotation.Nullable;
import org.apache.commons.io.IOUtils;
import org.apache.hc.client5.http.classic.methods.ClassicHttpRequests;
import org.apache.hc.client5.http.classic.methods.HttpUriRequest;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.ProtocolException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.HttpEntities;
import org.apache.hc.core5.http.io.entity.InputStreamEntity;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import ru.shuffle.smev.gateway.client.dto.ApiHeaders;
import ru.shuffle.smev.gateway.client.dto.AttachDto;
import ru.shuffle.smev.gateway.client.dto.AttachSpecificationDto;
import ru.shuffle.smev.gateway.client.dto.MessageDto;
import ru.shuffle.smev.gateway.client.dto.MessageRequestDto;
import ru.shuffle.smev.gateway.client.dto.MessageResponseDto;
import ru.shuffle.smev.gateway.client.dto.NotificationDto;
import ru.shuffle.smev.gateway.client.dto.ResponseDto;

public class SmevGatewayClient implements AutoCloseable {

    private static final Charset CHARSET = StandardCharsets.UTF_8;

    private static final ObjectMapper OBJECT_MAPPER = createObjectMapper();

    private static final ObjectWriter PRETTY_WRITER = OBJECT_MAPPER.writer().withDefaultPrettyPrinter();

    private static HttpEntity createJsonEntity(Object value) throws JsonProcessingException {
        return HttpEntities.create(OBJECT_MAPPER.writeValueAsBytes(value), ContentType.APPLICATION_JSON);
    }

    private static ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(SerializationFeature.FAIL_ON_SELF_REFERENCES, true);
        mapper.configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
        mapper.configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false);
        mapper.setSerializationInclusion(Include.NON_EMPTY);
        mapper.setDefaultPropertyInclusion(Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        mapper.registerModule(new Jdk8Module());
        mapper.setVisibility(mapper.getSerializationConfig().getDefaultVisibilityChecker()
            .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
            .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
            .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
            .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
        return mapper;
    }

    public static void dumpJson(Object value) {
        try {
            String json = PRETTY_WRITER.writeValueAsString(value);
            System.out.println(json);
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private static <R> ResponseDto<R> parseResponseDto(CloseableHttpResponse httpResponse, Class<R> resultType) throws Exception {
        String bodyString = httpResponse.getEntity() == null ? null : EntityUtils.toString(httpResponse.getEntity(), CHARSET);
        if (bodyString != null && bodyString.startsWith("{")) {
            JavaType responseType = OBJECT_MAPPER.getTypeFactory().constructParametricType(ResponseDto.class, resultType);
            ResponseDto<R> response = OBJECT_MAPPER.readValue(bodyString, responseType);
            readResponseHeadersAndStatusCode(response, httpResponse);
            return response;
        }
        else {
            throw new RuntimeException(String.format(
                "Bad response: code:%s, string:%s, body:%s",
                httpResponse.getCode(),
                httpResponse.getReasonPhrase(),
                bodyString));
        }
    }

    private static void readResponseHeadersAndStatusCode(ResponseDto<?> response, CloseableHttpResponse httpResponse) throws ProtocolException {
        response.responseCode = httpResponse.getCode();
        response.mnemonics = httpResponse.getHeader(ApiHeaders.MNEMONICS).getValue();
        response.state = Optional.ofNullable(httpResponse.getHeader(ApiHeaders.STATE)).map(Header::getValue).orElse(null);
    }

    private static void setHeaders(HttpUriRequest httpRequest, String authToken, String mnemonics, @Nullable String state) {
        setHeaders(httpRequest, authToken, mnemonics, state, null);
    }

    private static void setHeaders(HttpUriRequest httpRequest,
            String authToken, String mnemonics, @Nullable String state, @Nullable Boolean testLoopback) {
        httpRequest.setHeader(ApiHeaders.AUTH, authToken);
        httpRequest.setHeader(ApiHeaders.MNEMONICS, mnemonics);
        if (state != null) {
            httpRequest.setHeader(ApiHeaders.STATE, state);
        }
        if (testLoopback != null) {
            httpRequest.setHeader(ApiHeaders.IS_TEST_LOOPBACK, testLoopback);
        }
    }

    private final CloseableHttpClient client;

    private final String endPoint;

    public SmevGatewayClient(String endPoint) {
        this.endPoint = endPoint;
        client = HttpClients.createDefault();
    }

    public ResponseDto<NotificationDto> ackNotification(String authToken, String mnemonics, @Nullable String state, long notificationId) {
        try {
            HttpUriRequest httpRequest = ClassicHttpRequests.post(endPoint + "/api/v2/notification/" + notificationId);
            setHeaders(httpRequest, authToken, mnemonics, state);
            try (CloseableHttpResponse httpResponse = client.execute(httpRequest)) {
                return parseResponseDto(httpResponse, NotificationDto.class);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        IOUtils.closeQuietly(client, null);
    }

    public ResponseDto<AttachDto> createAttach(String authToken, String mnemonics, @Nullable String state, AttachSpecificationDto attachSpecification) {
        try {
            HttpUriRequest httpRequest = ClassicHttpRequests.post(endPoint + "/api/v2/attach");
            setHeaders(httpRequest, authToken, mnemonics, state);
            httpRequest.setEntity(createJsonEntity(attachSpecification));
            try (CloseableHttpResponse httpResponse = client.execute(httpRequest)) {
                return parseResponseDto(httpResponse, AttachDto.class);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseDto<MessageDto> createMessageRequest(String authToken, String mnemonics, @Nullable String state, MessageRequestDto messageRequest) {
        return createMessageRequest(authToken, mnemonics, state, null, messageRequest);
    }

    public ResponseDto<MessageDto> createMessageRequest(String authToken, String mnemonics,
            @Nullable String state, @Nullable Boolean testLoopback, MessageRequestDto messageRequest) {
        try {
            HttpUriRequest httpRequest = ClassicHttpRequests.post(endPoint + "/api/v2/message/request");
            setHeaders(httpRequest, authToken, mnemonics, state, testLoopback);
            httpRequest.setEntity(createJsonEntity(messageRequest));
            try (CloseableHttpResponse httpResponse = client.execute(httpRequest)) {
                return parseResponseDto(httpResponse, MessageDto.class);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseDto<MessageDto> createMessageResponse(String authToken, String mnemonics, @Nullable String state, MessageResponseDto messageResponse) {
        return createMessageResponse(authToken, mnemonics, state, null, messageResponse);
    }

    public ResponseDto<MessageDto> createMessageResponse(String authToken, String mnemonics,
            @Nullable String state, @Nullable Boolean testLoopback, MessageResponseDto messageResponse) {
        try {
            HttpUriRequest httpRequest = ClassicHttpRequests.post(endPoint + "/api/v2/message/response");
            setHeaders(httpRequest, authToken, mnemonics, state, testLoopback);
            httpRequest.setEntity(createJsonEntity(messageResponse));
            try (CloseableHttpResponse httpResponse = client.execute(httpRequest)) {
                return parseResponseDto(httpResponse, MessageDto.class);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseDto<Void> downloadAttachContent(String authToken, String mnemonics, @Nullable String state, long attachId, OutputStream outputStream) {
        try {
            HttpUriRequest httpRequest = ClassicHttpRequests.get(endPoint + "/api/v2/attach/content/" + attachId);
            setHeaders(httpRequest, authToken, mnemonics, state);
            try (CloseableHttpResponse httpResponse = client.execute(httpRequest)) {
                if (httpResponse.getCode() == 200) {
                    IOUtils.copy(httpResponse.getEntity().getContent(), outputStream);
                    ResponseDto<Void> response = new ResponseDto<>();
                    response.success = true;
                    readResponseHeadersAndStatusCode(response, httpResponse);
                    return response;
                }
                else {
                    return parseResponseDto(httpResponse, Void.class);
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseDto<AttachDto> findAttachById(String authToken, String mnemonics, @Nullable String state, long attachId) {
        try {
            HttpUriRequest httpRequest = ClassicHttpRequests.get(endPoint + "/api/v2/attach/" + attachId);
            setHeaders(httpRequest, authToken, mnemonics, state);
            try (CloseableHttpResponse httpResponse = client.execute(httpRequest)) {
                return parseResponseDto(httpResponse, AttachDto.class);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseDto<MessageDto> findMessageById(String authToken, String mnemonics, @Nullable String state, long messageId) {
        try {
            HttpUriRequest httpRequest = ClassicHttpRequests.get(endPoint + "/api/v2/message/" + messageId);
            setHeaders(httpRequest, authToken, mnemonics, state);
            try (CloseableHttpResponse httpResponse = client.execute(httpRequest)) {
                return parseResponseDto(httpResponse, MessageDto.class);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseDto<NotificationDto> findNotificationById(String authToken, String mnemonics, @Nullable String state, long notificationId) {
        try {
            HttpUriRequest httpRequest = ClassicHttpRequests.get(endPoint + "/api/v2/notification/" + notificationId);
            setHeaders(httpRequest, authToken, mnemonics, state);
            try (CloseableHttpResponse httpResponse = client.execute(httpRequest)) {
                return parseResponseDto(httpResponse, NotificationDto.class);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseDto<NotificationDto[]> listNotifications(String authToken, String mnemonics, @Nullable String state, int limit, int offset) {
        try {
            HttpUriRequest httpRequest = ClassicHttpRequests.get(String.format("%s/api/v2/notification/list/?limit=%s&offset=%s", endPoint, limit, offset));
            setHeaders(httpRequest, authToken, mnemonics, state);
            try (CloseableHttpResponse httpResponse = client.execute(httpRequest)) {
                return parseResponseDto(httpResponse, NotificationDto[].class);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseDto<AttachDto> uploadAttachContent(String authToken, String mnemonics, @Nullable String state, long attachId, InputStream content) {
        try {
            HttpUriRequest httpRequest = ClassicHttpRequests.post(endPoint + "/api/v2/attach/content/" + attachId);
            setHeaders(httpRequest, authToken, mnemonics, state);
            httpRequest.setEntity(new InputStreamEntity(content, ContentType.APPLICATION_OCTET_STREAM));
            try (CloseableHttpResponse httpResponse = client.execute(httpRequest)) {
                return parseResponseDto(httpResponse, AttachDto.class);
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
