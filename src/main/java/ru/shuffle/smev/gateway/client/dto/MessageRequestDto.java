package ru.shuffle.smev.gateway.client.dto;

import java.time.Instant;
import java.util.UUID;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class MessageRequestDto {

    //    @Schema(description = "Идентификатор первичного сообщения опциональный элемент, "
    //      + "указывающий на первичный MessageID в цепочке запросов одной бизнес-транзакции. При отправке первичного запроса "
    //+ "ReferenceMessageID и MessageID совпадают")
    @Nullable
    public UUID referenceMessageId;

    //@Schema(description = "Код транзакции , опциональный элемент, указывающий на транзакцию оказания государственной услуги или выполнения "
    //        + "государственной функции, в рамках которой посылается запрос. Если в транзакции запрос является первым, то данный "
    //  + "элемент следует заполнять значением, полученным в СГКТ. Если в транзакции запрос является промежуточным, то данный элемент "
    //      + "следует заполнять значением, полученным в запросе, на основании которого посылается данный запрос")
    @Nullable
    public String transactionCode;

    //    @Schema(description = "(timestamp-seconds UTC) - время, до истечения которого запрос является для ИС инициатора актуальным")
    @Nullable
    public Instant eol;

    //  @Schema(description = "xml тело запроса (&lt;?xml заголовок обязателен, корневой таг &lt;MessagePrimaryContent&gt;)", required = true)
    @NotNull
    public String messagePrimaryContent;

    //    @Schema(description = "опциональный xml блок атрибутов бизнес-процесса. (&lt;?xml заголовок обязателен, корневой таг &lt;BusinessProcessMetadata&gt;")
    @Nullable
    public String businessProcessMetadata;

    //  @Schema(description = "список id аттачей")
    @Nullable
    public long[] attachIds;

}