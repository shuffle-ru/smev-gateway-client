package ru.shuffle.smev.gateway.client.dto;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class ErrorDto {

    //    @Schema(description = "Код ошибки", required = true)
    @NotNull
    public ErrorCodeDto code;

    //  @Schema(description = "Описание ошибки")
    @Nullable
    public String message;

}
