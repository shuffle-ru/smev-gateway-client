package ru.shuffle.smev.gateway.client.dto;

public enum ErrorCodeDto {
        //        @Schema(description = "Ошибка авторизации")
        NOT_AUTH,
        //      @Schema(description = "Ошибка в структуре запроса")
        BAD_REQUEST,
        //    @Schema(description = "Сущность не найдена")
        NOT_FOUND,
        //  @Schema(description = "Другая ошибка")
        SERVER_ERROR;
}