package ru.shuffle.smev.gateway.client.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class AttachSpecificationDto {

    //@Schema(description = "имя файла ([a-zA-Z0-9_\\\\-\\\\.] - английские символы, цифры, и знаки -_.", required = true)
    @NotBlank
    @Pattern(regexp = "[a-zA-Z0-9_\\-\\.]+")
    public String fileName;

    //@Schema(description = "content/type attach для СМЭВ", required = true)
    @NotBlank
    public String contentType;

    //@Schema(description = "описание вложения (опционально)")
    public String description;

    //@Schema(description = "вид сведений, URN проткола обмена")
    public String exchangeProtocolURN;

}
