package ru.shuffle.smev.gateway.client.dto;

public enum MessageStatusCategoryDto {
        //        @Schema(description = "Запрос с таким Id не найден в БД СМЭВ")
        DOES_NOT_EXIST,
        //      @Schema(description = "Запрос находится в очереди на асинхронную валидацию")
        REQUEST_IS_QUEUED,
        //    @Schema(description = "Запрос доставляется поставщику")
        REQUEST_IS_ACCEPTED_BY_SMEV,
        //  @Schema(description = "Запрос не прошёл асинхронную валидацию")
        REQUEST_IS_REJECTED_BY_SMEV,
        //@Schema(description = "Обрабатывается поставщиком сервиса")
        UNDER_PROCESSING,
        //@Schema(description = "Запрос выполнен ответ находится в очереди СМЭВ")
        RESPONSE_IS_ACCEPTED_BY_SMEV,
        //@Schema(description = "Запрос отменён сервисом")
        CANCELLED;
}