package ru.shuffle.smev.gateway.client.dto;

public enum MessageStatusDto {
        //        @Schema(description = "Новый")
        NEW,
        //      @Schema(description = "Ошибка от СМЭВ при отправке сообщения в СМЭВ")
        FAILED,
        //    @Schema(description = "Отправленный")
        SENDED,
        //  @Schema(description = "Ошибка от СМЭВ при отправке - отложен для повторной отправки")
        FAILED_RETRY,
        //        @Schema(description = "Полученный")
        RECEIVED;
}