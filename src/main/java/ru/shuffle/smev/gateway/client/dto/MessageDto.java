package ru.shuffle.smev.gateway.client.dto;

import java.time.Instant;
import java.util.UUID;
import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class MessageDto {

    //    @Schema(description = "Id сообщения в smev_gateway", required = true)
    public long id;

    ///  @Schema(description = "Дата создания сообщения (timestamp seconds UTC)", required = true)
    @NotNull
    public Instant createDate;

    //@Schema(description = "Дата обновления сообщения (timestamp seconds UTC)", required = true)
    @NotNull
    public Instant updateDate;

    //@Schema(description = "Дата отправки сообщения в СМЭВ (timestamp seconds UTC)")
    @Nullable
    public Instant sendedDate;

    //@Schema(description = "Дата получения сообщения от СМЭВ (timestamp seconds UTC)")
    @Nullable
    public Instant receivedDate;

    //@Schema(description = "id SMEV GATEWAY сообщение на которое пришёл ответ")
    @Nullable
    public Long originalMessageId;

    //@Schema(description = "ID сообщение СМЭВ")
    @Nullable
    public UUID smevId;

    //@Schema(description = "ID сообщения, адресата ответа для СМЭВ")
    @Nullable
    public UUID smevOriginalMessageId;

    //@Schema(description = "ID сообщения, который породил цепочку запросов СМЭВ")
    @Nullable
    public UUID smevReferenceMessageId;

    //@Schema(description = "Тип сообщения", required = true)
    @NotNull
    public MessageTypeDto type;

    //@Schema(description = "Cтатус сообщения", required = true)
    @NotNull
    public MessageStatusDto status;

    ///@Schema(description = "Мнемоника", required = true)
    @NotBlank
    public String mnemonics;

    //@Schema(description = "Протокол обмена (Виды сведений)", required = true)
    @NotBlank
    public String exchangeProtocolURN;

    //@Schema(description = "JSON запрос к smev-gateway")
    @Nullable
    public String requestJson;

    //@Schema(description = "SOAP запрос к СМЭВ")
    @Nullable
    public String requestSoap;

    //@Schema(description = "SOAP ответ от СМЭВ при запросе")
    @Nullable
    public String responseSoap;

    //@Schema(description = "ACK SOAP запрос к СМЭВ")
    @Nullable
    public String requestAckSoap;

    //@Schema(description = "ACK SOAP ответ от СМЭВ при запросе")
    @Nullable
    public String responseAckSoap;

    //@Schema(description = "Лог")
    @Nullable
    public String log;

    //@Schema(description = "Вложения")
    @Nullable
    public AttachDto[] attaches;

}