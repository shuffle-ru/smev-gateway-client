package ru.shuffle.smev.gateway.client.dto;

import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

public class ResponseDto<R> {

    //    @Schema(description = "Успешен ли запрос", required = true)
    public boolean success;

    //  @Schema(description = "Ошибка")
    @Nullable
    public ErrorDto error;

    //    @Schema(description = "Результат", required = true)
    @Nullable
    @Valid
    public R result;

    @Nullable
    public String state;

    @NotBlank
    public String mnemonics;

    public int responseCode;

}
