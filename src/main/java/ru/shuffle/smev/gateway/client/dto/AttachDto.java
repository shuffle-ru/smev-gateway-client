package ru.shuffle.smev.gateway.client.dto;

import java.time.Instant;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class AttachDto {

    //@Schema(description = "id файла", required = true)
    public long id;

    //@Schema(description = "Дата создания (timestamp seconds UTC)", required = true)
    @NotNull
    public Instant createDate;

    //@Schema(description = "Дата обновления (timestamp seconds UTC)", required = true)
    @NotNull
    public Instant updateDate;

    //@Schema(description = "Дата, когда клиент загрузил attach в GW (timestamp seconds UTC)")
    public Instant clientUploadDate;

    //@Schema(description = "Дата, когда attach был отправлен СМЭВ (timestamp seconds UTC)")
    public Instant smevUploadDate;

    //@Schema(description = "Дата, до который attach будет заблокирован (к примеру если СМЭВ ftp не доступен) (timestamp seconds UTC)")
    public Instant lockUntilDate;

    //@Schema(description = "имя файла ([a-zA-Z0-9_\\\\-\\\\.] - английские символы, цифры, и знаки -_.", required = true)
    @NotBlank
    @Pattern(regexp = "[a-zA-Z0-9_\\-\\.]+")
    public String fileName;

    //@Schema(description = "описание вложения (опционально)")
    public String description;

    //@Schema(description = "вид сведений, URN проткола обмена")
    public String exchangeProtocolURN;

    //@Schema(description = "content/type файла для СМЭВ", required = true)
    @NotBlank
    public String contentType;

    //@Schema(description = "размер файла в байтах")
    public Long contentLength;

    //@Schema(description = "Hash файла (HEX)")
    public String hash;

    //@Schema(description = "PKCS7 подпись файла (HEX)")
    public String signature;

    //@Schema(description = "Прошла ли проверку подпись файла")
    public Boolean signatureVerify;

    //@Schema(description = "id сообщения к которому был приаттачен (один файл можно приаттачить один раз)")
    public Long messageId;

    //@Schema(description = "smev uuid (UUID дирекории в СМЭВ FTP)")
    public UUID smevUUID;

    //@Schema(description = "информация сертификата - которым был подписан файл")
    public String certificateInfo;

    //@Schema(description = "тип вложения")
    public AttachTypeDto type;

    //@Schema(description = "статус вложения")
    public AttachStatusDto status;

}
