package ru.shuffle.smev.gateway.client.dto;

import java.util.UUID;
import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class MessageResponseDto {

    //    @Schema(description = "id запроса из СМЭВ - на который отправляется запрос", required = true)
    @NotNull
    public UUID originalMessageId;

    //  @Schema(description = "Адрес доставки ответа (//To), обязательный элемент, в который должно быть скопировано содержимое "
    //    + "элемента //GetRequestResponse/RequestMessage/ Request/ReplyTo запроса, на который отправляется ответ", required = true)
    @NotBlank
    public String to;

    //@Schema(description = "xml тело запроса (&lt;?xml заголовок обязателен, корневой таг &lt;MessagePrimaryContent&gt;)")
    @Nullable
    public String messagePrimaryContent;

    @Nullable
    @Valid
    //@Schema(description = "Запрос отклонён")
    public MessageRejectedDto[] requestRejected;

    @Nullable
    @Valid
    //@Schema(description = "Статус запроса, может быть использован для информирования о статусе обработки запроса")
    public MessageRequestStatusDto requestStatus;

    @Nullable
    @Valid
    //@Schema(description = "Статус обработки сообщения (запроса либо ответа на запрос) в СМЭВ. "
    //  + "Данный элемент предназначен для использования только СМЭВ для информирования ИС "
    //        + "о статусе обработки их сообщений в СМЭВ. В то же время данный элемент не предназначен "
    //      + "для использования ИС ответчиков для информирования ИС инициаторов о статусах обработки их сообщений. "
    //    + "В случае использования ИС ответчика данного элемента в отправляемом ею ответе СМЭВ вернёт данной ИС ошибку.")
    public MessageAsyncProcessingStatusDto asyncProcessingStatus;

    //@Schema(description = "список id аттачей")
    @Nullable
    public long[] attachIds;

}