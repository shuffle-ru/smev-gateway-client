package ru.shuffle.smev.gateway.client.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class MessageRejectedDto {
    //    @Schema(description = "Описание причины отклонения запроса, в человекочитаемом виде", required = true)

    @NotBlank
    public String rejectionReasonDescription;

    //    @Schema(description = "Код причины отклонения", required = true)
    @NotNull
    public MessageRejectionReasonCodeDto rejectionReasonCode;

}