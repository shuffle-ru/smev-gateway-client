package ru.shuffle.smev.gateway.client.dto;

public enum MessageRejectionReasonCodeDto {
        ACCESS_DENIED,
        NO_DATA,
        UNKNOWN_REQUEST_DESCRIPTION,
        FAILURE;
}