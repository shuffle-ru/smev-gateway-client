package ru.shuffle.smev.gateway.client.dto;

import javax.annotation.Nullable;

public class MessageFaultDto {

    //@Schema(description = "Код ошибки")
    @Nullable
    public String code;

    //@Schema(description = "Описание ошибки")
    @Nullable
    public String description;

}