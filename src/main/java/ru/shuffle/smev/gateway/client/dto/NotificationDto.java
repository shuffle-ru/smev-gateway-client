package ru.shuffle.smev.gateway.client.dto;

import java.time.Instant;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class NotificationDto {

    //@Schema(description = "id оповещения - в smev_gateway", required = true)
    public long id;

    //@Schema(description = "Message.Id - в smev_gateway", required = true)
    public long messageId;

    //@Schema(description = "Протокол обмена - виды сведений", required = true)
    @NotBlank
    public String exchangePorotocolURN;

    //@Schema(description = "Код мнемоники", required = true)
    @NotBlank
    public String mnemonics;

    //    @Schema(description = "Дата создания оповещения (timestamp seconds UTC)", required = true)
    @NotNull
    public Instant createDate;

    //  @Schema(description = "Тип оповещения", required = true)
    @NotNull
    public NotificationTypeDto type;

}